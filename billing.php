<?php include 'include/head.php';?>
<?php include 'include/nav.php';?>
<?php
// delete struk if back button click on billout.php
if (!isset($_GET['back'])){$_GET['back'] = 0;}
if ($_GET['back'] == 1){
	$con->query("delete from struk where id_struk = '$_GET[idstruk]'");
	$con->query("delete from bk where id_struk = '$_GET[idstruk]'");
}
?>
  <div class="content2 container">
     <?php echo $notice; ?>
    <div class="center">
  <h3>Payment Bill</h3>
    </div>

  <?php 
  if (isset($_POST['updateBill'])){
    $id = $_POST['id'];
    $qty = $_POST['qty'];
	// cek qty
	$sqsq = "SELECT id,unit FROM inventeries WHERE id = '$id'";
	$result1 = $con->query($sqsq);
	$data11 = $result1->fetch_assoc();
	if($data11['unit'] == 1){
		echo 
     	"<script>
     		window.alert(\"Kuantiti melebihi maksimum\");
     	</script>
     	";
	}else {
		foreach ($_SESSION['bill'] as $key => $value){
			if($_SESSION['bill'][$key]['id'] == $id){
				$_SESSION['bill'][$key]['qty'] = $qty;
			}
		}
	}
  }
  	$i=0;$total = 0;
    ?>
    <br>
<table class="table table-striped table-hover">
    	<tr>
    		<th>#</th>
    		<th >Item</th>
    		<th>Price</th>
        <th></th>
    		<th>Qty</th>
    	</tr>
    <?php
    foreach ($_SESSION['bill'] as $row) 
    {
      $i++;
      echo "<tr>";
      echo "<td>$i</td>";
      echo "<td id='$row[id]'>$row[name]</td>";
      echo "<td>".rupiah($row['price'])."</td>";
      echo "<td><a href='called.php?remove=$row[id]'><button class='btn btn-primary'>Remove</button></a></td>";
      echo "<td> 
            <form method='POST' action='billing.php#$row[id]'>
            <input type='hidden' value='$row[id]' name='id'>
            <input type='number' min='1' class='form-control input-sm pull-left' value ='$row[qty]' style='width:88px;height: 35px;' name='qty'>  
			<button type='submit' name='updateBill' style='margin-left:2px' class='btn'>Update</button>
            </form>
            </td>";
      echo "</tr>";
      $total = $total + $row['price']*$row['qty'];
    }
  ?>
  <tr>
    <td colspan="2">Total Bill</td>
    <td colspan="2"><strong><?php echo rupiah($total) ?></strong></td>
    <td><button class="btn btn-lg btn-primary btn-block" data-toggle="modal" data-target="#billOut">BAYAR</button></td>
  </tr>
</table>
  </div>
</div>

<div id="billOut" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Receipt Payment</h4>
      </div>
      <div class="modal-body"> 
	  <form method="POST" action="billout.php" onSubmit="validasi()">
        <div style="width: 77%;margin: auto;">
          <div class="form-group">
            <label for="some" class="col-form-label">Name</label>
            <input type="text" name="name" class="form-control" id="some" required>
          </div>
           <div class="form-group">
            <label for="some" class="col-form-label">Cash</label>
            <input type="text" name="cash" class="form-control" id="cash" required>
          </div>
          <div class="form-group">
            <label for="some" class="col-form-label">Notes / Contact</label>
            <input type="text" name="contact" class="form-control" id="some" required>
          </div>
       
        </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" name="billInfo">View Bill</button>
      </div>
    </form>
    </div>

  </div>
</div>
	<script>
	function validasi() {
		var totbil = <?php echo $total ?>;
		var cash = document.getElementById("cash").value;
		var hasil1 = totbil == cash;
		var hasil2 = totbil > cash;
		if(cash >= totbil){
			return true;
		}else{
			confirm("Uang anda kurang, apakah ingin lanjut?");
		}
	}
		
	</script>
<?php include 'include/footer.php';?>