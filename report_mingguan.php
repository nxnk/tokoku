<?php include 'include/head.php';?>
<?php include 'include/nav.php';?>
  <div class="content container">
  <a href="reports.php" class="btn btn-primary float-right"><i class="icon icon-back"></i> Kembali</a>
  <br/><br/>
  <div class="tableBox" >
    <table id="dataTable" class="table table-responsive table-hover">
      <thead>
        <th>#</th>
        <th>Tahun</th>
        <th>Minggu Ke</th>
        <th>Jumlah Transaksi</th>
      </thead>
     <tbody>
      <?php $i=0;
          $array = $con->query("SELECT id, YEARWEEK(date) AS tahun_minggu, COUNT(*) AS jumlah_trx FROM bk GROUP BY YEARWEEK(date);");
        while ($row = $array->fetch_assoc()) 
        { 
          $i=$i+1;
          $id = $row['id'];
		  $tahun = substr($row['tahun_minggu'], 0,4);
		  $mingguan = substr($row['tahun_minggu'], 4);
        ?>
          <tr>
            <td><?php echo $i; ?></td>
            <td><?php echo $tahun; ?></td>
            <td><?php echo $mingguan; ?></td>
            <td><!--<a href="report_mingguan_detail.php?date=<?php echo $row['date']; ?>">--><?php echo $row['jumlah_trx']; ?><!--</a>--></td>
          </tr>
      <?php
        }
       ?>
     </tbody>
    </table>

  </div>                      

  </div>  
    <?php include 'include/footer.php';?>