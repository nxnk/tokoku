<?php include 'include/head.php';?>
<?php include 'include/nav.php';?>
  <div class="content container">
  <a href="reports.php" class="btn btn-primary float-right"><i class="icon icon-back"></i> Kembali</a>
  <br/><br/>
  <div class="tableBox" >
    <table id="dataTable" class="table table-responsive table-hover">
      <thead>
        <th>#</th>
        <th>Id Barang</th>
        <th>Name</th>
        <th>Amount</th>
        <th>Id Struk</th>
        <th>Staff</th>
        <th>Date</th>
        <th></th>
        
      </thead>
     <tbody>
      <?php $i=0;
          $array = $con->query("
		  SELECT 
bk.id,
inventeries.name,
bk.userId,
bk.amount,
bk.id_struk,
bk.date
FROM bk
LEFT JOIN inventeries
ON bk.id_barang = inventeries.id
		  ORDER BY bk.date DESC");
        while ($row = $array->fetch_assoc()) 
        { 
          $i=$i+1;
          $id = $row['id'];
        ?>
          <tr>
            <td><?php echo $i; ?></td>
            <td><?php echo $row['id']; ?></td>
            <td><?php echo $row['name']; ?></td>
            <td><?php echo $row['amount']; ?></td>
            <td><?php echo $row['id_struk']; ?></td>
            <td><?php echo getAdminName($row['userId']); ?></td>
            <td><?php echo $row['date']; ?></td>
			<td>
				<a onclick="return konfirmasi()" href="delete.php?item_bk=<?php echo $row['id'] ?>"><button class='btn btn-danger'>Delete Item</button></a>
			</td>
          </tr>
      <?php
        }
       ?>
     </tbody>
    </table>

  </div>                      

  </div>  
    <?php include 'include/footer.php';?>