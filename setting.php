<?php include 'include/head.php';?>
<?php include 'include/nav.php'; ?>
<?php
if($_SESSION['level'] != 1)
{
  ?><script>window.location.href = "index.php";</script><?php
}
?>
<div class="container text-center">
<div class="row">
<div class="columns">

    <div class="column col-1 p-3 p-md-5">
		</div>
    <div class="column col-10 p-3 p-md-5">
	<div class="panel">
  <div class="panel-header">
    <div class="panel-title"><h3>Setting</h3></div>
  </div>
  <div class="panel-nav">
 Select Setting menu
  </div>
  <div class="panel-body">
  <ul class="nav">
  <li class="nav-item">
    <a href="sitesetting.php"><i class="icon icon-location"></i> Store</a>
  </li>
  <li class="nav-item">
    <a href="accountSetting.php"><i class="icon icon-mail"></i> Login</a>
  </li>
  <li class="nav-item">
    <a href="tambahUser.php"><i class="icon icon-mail"></i> Tambah User</a>
  </li>
</ul>
  </div>

</div>
	</div>
</div>
</div>
</div>
<?php include 'include/footer.php';?>