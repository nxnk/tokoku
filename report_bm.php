<?php include 'include/head.php';?>
<?php include 'include/nav.php';?>
  <div class="content container">
  <a href="reports.php" class="btn btn-primary float-right"><i class="icon icon-back"></i> Kembali</a>
  <br/><br/>
  <div class="tableBox" >
    <table id="dataTable" class="table table-responsive table-hover">
      <thead>
        <th>#</th>
        <th>Id Barang</th>
        <th>Name</th>
        <th>Amount</th>
        <th>Staff</th>
        <th>Date</th>
        <th></th>
        
      </thead>
     <tbody>
      <?php $i=0;
          $array = $con->query("
		  SELECT 
bm.id,
inventeries.name,
bm.userId,
bm.amount,
bm.date
FROM bm
LEFT JOIN inventeries
ON bm.id_barang = inventeries.id
		  ORDER BY bm.date DESC");
        while ($row = $array->fetch_assoc()) 
        { 
          $i=$i+1;
          $id = $row['id'];
        ?>
          <tr>
            <td><?php echo $i; ?></td>
            <td><?php echo $row['id']; ?></td>
            <td><?php echo $row['name']; ?></td>
            <td><?php echo $row['amount']; ?></td>
            <td><?php echo getAdminName($row['userId']); ?></td>
            <td><?php echo $row['date']; ?></td>
			<td>
				<a onclick="return konfirmasi()" href="delete.php?item_bm=<?php echo $row['id'] ?>"><button class='btn btn-danger'>Delete Stok Masuk</button></a>
			</td>
          </tr>
      <?php
        }
       ?>
     </tbody>
    </table>

  </div>                      

  </div>  
    <?php include 'include/footer.php';?>