<?php include 'include/head.php';?>
<?php include 'include/nav.php';?>
    <div class="container">
	<?php echo $notice ?>
Kategori
      <button class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#addIn"><i class="icon icon-plus"></i> Kategori Baru</button> 
     <br/><br/>
  <?php 
  	$i=0;
    $array = $con->query("select * from categories");
    ?>
    <br>
<table class="table table-striped table-hover" width="50%">
    <tr>
    		<th>#</th>
    		<th>Name</th>
    		<th>Deskripsi</th>
    		<th>Products</th>
    		<th>Set</th>
    	</tr>
    
	<?php
    while ($row = $array->fetch_assoc()){
		$i++;
		$array2 = $con->query("select count(*) as qty from categories where id = '$row[id]'");
		$row2 = $array2->fetch_assoc();
  ?>
    <tr>
    	<td><?php echo $i ?></td>
    	<td><?php echo $row['name']; ?></td>
    	<td><?php echo $row['description']; ?></td>
    	<td><?php echo $row2['qty']; ?></td>
    	<td>
			<a href="manageCat_Ubah.php?id=<?php echo $row['id'] ?>"><button class="btn btn-primary">Ubah</button></a>
			<a onclick="return konfirmasi()" href="delete.php?category=<?php echo $row['id'] ?>"><button class="btn btn-danger">Delete</button></a>
		</td>
    </tr>
	<?php
    }
	
	?>
   </table>
  </div>

<div id="addIn" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Kategori Baru</h4>
      </div>
      <div class="modal-body"> <form method="POST" enctype="multipart/form-data">
        <div style="width: 77%;margin: auto;">
         
          <div class="form-group">
            <label for="some" class="col-form-label">Nama Kategori</label>
            <input type="text" name="name" class="form-control" id="name" required>
          </div>
		  <div class="form-group">
            <label for="some" class="col-form-label">Deskripsi Kategori</label>
			<input type="text" name="desc" class="form-control" id="desc" required>
          </div>
          
       
        </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary" name="safeIn">Simpan</button>
      </div>
    </form>
    </div>

  </div>
</div>

<?php include 'include/footer.php';?>