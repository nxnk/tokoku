<?php include 'include/head.php';?>
<?php include 'include/nav.php';?>
<?php 
	$id = $_GET['id'];
	$array = $con->query("SELECT 
i.id,
i.name,
c.name as cat_name,
i.description
FROM categories c
LEFT JOIN inventeries i
ON c.id = i.catId
WHERE i.id = '$id'");
	$idid = $array->fetch_assoc();
?>
  <div class="content2 container">
<div class="columns">
    <div class="column col-3"></div>
<div class="column col-6">
      <h4>Ubah Produk</h4>
	  <a href="products.php" class="btn btn-primary"><i class="icon icon-back"></i> Kembali</a>
	  <hr>
      <form method="POST">
		  <div class="form-group">
				<label for="some" class="col-form-label">Select Category</label>
				<select class="form-control" required name="catId">
					<option selected disabled value="<?php echo $idid['catId'];?>"><?php echo $idid['cat_name'];?></option>
					<?php getAllCat(); ?>
				</select>
			</div>
          <div class="form-group">
            <label for="some" class="col-form-label">Nama Produk</label>
            <input type="text" name="name" class="form-control" id="name" value="<?php echo $idid['name'];?>" required>
          </div>
          <div class="form-group">
            <label for="some" class="col-form-label">Deskripsi</label>
            <input type="text" name="desc" class="form-control" id="description" value="<?php echo $idid['description'];?>" required>
          </div>
		  <input type="hidden" name="idprod" class="form-control" value="<?php echo $idid['id'];?>">
          <div class="center">
            <button type="submit" name="UpdateProd" class="btn btn-primary">Simpan</button>
            <button type="reset" class="btn btn-warning">Reset</button>
          </div>
        </form>
    </div>
</div>
<?php include 'include/footer.php';?>