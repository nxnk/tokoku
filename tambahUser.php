<?php include 'include/head.php';?>
<?php include 'include/nav.php';?>
  <div class="content2 container">

    <?php echo $notice ?>
<div class="columns">
    <div class="column col-3"></div>
<div class="column col-6">
      <h4>Add New User</h4><hr>
      <form method="POST">
          <div class="form-group">
            <label for="some" class="col-form-label">Nama</label>
            <input type="text" name="name" class="form-control" id="some" required>
          </div>
          <div class="form-group">
            <label for="some" class="col-form-label">Email</label>
            <input type="email" name="email"  class="form-control" id="some" required>
          </div>
          <div class="form-group">
            <label for="some" class="col-form-label">Password</label>
            <input type="password" name="password"  class="form-control" id="some" required>
          </div>
          <div class="center">
            <button type="submit" name="saveUser" class="btn btn-primary">Save</button>
            <button type="reset" class="btn btn-success">Reset</button>
          </div>
        </form>
    </div>
</div>

<?php include 'include/footer.php';?>