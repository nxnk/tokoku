<?php include 'include/head.php';?>
<?php include 'include/nav.php';?>
  <div class="content2 container">

    <?php echo $notice ?>
<div class="columns">
    <div class="column col-3"></div>
<div class="column col-6">
      <h4>Add New Items</h4><hr>
      <form method="POST">
         
		    <div class="form-group">
				<label for="some" class="col-form-label">Select Category</label>
				<select class="form-control" required name="catId">
				  <option selected disabled value="">Please Select Category</option>
				<?php getAllCat(); ?>
				  
				</select>
			</div>
			<div class="form-group">
				<label for="some" class="col-form-label">Nama Barang</label>
				<input type="text" name="name" class="form-control" id="nameBrg" required>
			</div>
			<div class="form-group">
				<label for="some" class="col-form-label">Deskripsi Barang</label>
				<input type="text" name="desc" class="form-control" id="desc" required>
			</div>
          <div class="center">
            <button type="submit" name="saveProduct" class="btn btn-primary">Simpan</button>
            <button type="reset" class="btn btn-success">Reset</button>
          </div>
        </form>
    </div>
</div>
<?php include 'include/footer.php';?>