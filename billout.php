<?php
session_start();
error_reporting(0);
date_default_timezone_set('Asia/Jakarta');
if(!isset($_SESSION['userId']))
{
  header('location:login.php');
}
require 'assets/db.php';
require "assets/function.php";
$cash = $_POST['cash'];
function rupiah($angka){
	$hasil_rupiah = "Rp " . number_format($angka,0,'.',',');
	return $hasil_rupiah;
}
?>
<html>
<head>
<title>Faktur Pembayaran</title>
<style>
	* {
		font-size: 12px;
		font-family: 'Times New Roman';
	}
	td,
	th,
	tr,
	table {
		border-top: 1px black;
		border-top-style: solid;
		border-collapse: collapse;
	}
	.centered {
    text-align: center;
    align-content: center;
	}
	.right-align{text-align:right;}
	.namatoko {font-size:20px;}
	@media print {
		@page { 
			size: 58mm 100mm;
			margin: 1mm 1mm 1mm 1mm;
		}
		.tombolbol{	display: none; }
		.namatoko {font-size:20px;}
		.head-str, .nm-jln {font-size:11px;}
		td,
	th,
	tr,
	table {
		border-top: 1px black;
		border-top-style: dashed;
		border-collapse: collapse;
		margin: 0 auto;
	}
	}
</style>
</head>
<?php 
//1111tgl
$id = base_convert(microtime(false), 4, 10);
$idstruk = "11". $id . date("dmy")."0";
?>
<body class="receipt">
	<table border='0'>
		<tr>
		<td colspan="5" class="centered">
			</br><br/>
			<b class="namatoko"><?php echo siteName() ?></b>
			</br>
			<span class="nm-jln"><?php echo siteAlamat() ?></span>
			</br>
			<span class="head-str">
			No. : <?php echo $idstruk; ?>
			<br/>
			<?php echo date("d-m-Y H:i"); ?>
			</span>
			</br><br/><br/>
		</td>
		</tr>
		<?php
			$i=$total=0;
			foreach ($_SESSION['bill'] as $row) 
			{		  
				$jml = $row['price']*$row['qty'];
				$i++;
			  echo "
				<tr>
					<td colspan='4'>$row[name]</td>
				</tr>
				<tr>
					<td>".rupiah($row[price])."</td>
					<td class=\"right-align\">x $row[qty]</td>
					<td class=\"right-align\">".rupiah($jml)."</td>
				</tr>
			  ";
			  
			  $total = $total + $row['price']*$row['qty'];
			  if (!$con->query("insert into bk (id_barang,catID,name,amount,userId,id_struk) values ('$row[id]','$row[catid]','$row[name]','$row[qty]','$_SESSION[userId]', '$idstruk')")){echo "Error is:".$con->error;}
			}
		?>
		<tr>
			<td colspan="2">
				<div>Total : </div>
			</td>
			<td class="right-align">
				<?php echo rupiah($total); ?>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<div>Bayar : </div>
			</td>
			<td class="right-align">
				<?php echo rupiah($cash); ?>
			</td>
		</tr>
		<tr>
		<?php
			$totalchh = $total - $cash;
			if($total >= $cash){$whats = "Kurang";}else{$whats = "Kembali";}
		?>
			<td colspan="2">
				<div><?php echo $whats; ?> : </div>
			</td>
			<td class="right-align">
				<?php echo rupiah(abs($totalchh)); ?>
			</td>
		</tr>
		<td colspan="5" class="centered">
			<br/><br/><br/>
			****** TERIMAKASIH ******
			<div class="tombolbol">
				<button class="btn btn-primary" name="printnow" value="1" onclick="window.print()">Print</button>
				<a href="billing.php?idstruk=<?php echo $idstruk;?>&back=1"><button class="btn btn-success">Back</button></a>
				<a href="clear.php?idstruk=<?php echo $idstruk;?>&clear=1"><button class="btn btn-success">Clear</button></a>
				<?$_POST['submit_btn'];?>
			</div>
		</td>
	</table>
</body>
</html>
<?php 
if (!$con->query("insert into struk (id_struk,name,amount,item,userId) values ('$idstruk','$_POST[name]','$total','$i','$_SESSION[userId]')")){
		echo "Error is:".$con->error;
	}
/*if (isset($_POST['billInfo'])){
	unset($_SESSION['bill']);
	$_SESSION['bill'] = array();
}*/
 ?>