<?php include 'include/head.php';?>
<?php include 'include/nav.php';?>
<?php 
	$id = $_GET['id'];
	$array = $con->query("select * from categories where id = '$id'");
	$idid = $array->fetch_assoc();
?>
  <div class="content2 container">

    <?php echo $notice ?>
<div class="columns">
    <div class="column col-3"></div>
<div class="column col-6">
      <h4>Ubah Kategori</h4>
	  <a href="manageCat.php" class="btn btn-primary"><i class="icon icon-back"></i> Kembali</a>
	  <hr>
      <form method="POST">
          <div class="form-group">
            <label for="some" class="col-form-label">Nama Kategori</label>
            <input type="text" name="name" class="form-control" id="name" value="<?php echo $idid['name'];?>" required>
          </div>
          <div class="form-group">
            <label for="some" class="col-form-label">Deskripsi</label>
            <input type="text" name="description" class="form-control" id="description" value="<?php echo $idid['description'];?>" required>
          </div>
		  <input type="hidden" name="idkat" class="form-control" value="<?php echo $idid['id'];?>">
          <div class="center">
            <button type="submit" name="UpdateCat" class="btn btn-primary">Simpan</button>
            <button type="reset" class="btn btn-warning">Reset</button>
          </div>
        </form>
    </div>
</div>
<?php include 'include/footer.php';?>