-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 12, 2022 at 07:51 AM
-- Server version: 10.4.8-MariaDB-log
-- PHP Version: 7.4.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tokoku`
--
CREATE DATABASE IF NOT EXISTS `tokoku` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `tokoku`;

-- --------------------------------------------------------

--
-- Table structure for table `bk`
--

CREATE TABLE `bk` (
  `id` int(11) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `catID` int(11) NOT NULL,
  `name` varchar(222) NOT NULL,
  `amount` varchar(222) NOT NULL,
  `userId` int(11) NOT NULL,
  `id_struk` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `bk`
--
DELIMITER $$
CREATE TRIGGER `barang_keluar` AFTER INSERT ON `bk` FOR EACH ROW BEGIN
   UPDATE inventeries SET unit = unit - NEW.amount
   WHERE id = NEW.id_barang;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `barang_keluar_delete` AFTER DELETE ON `bk` FOR EACH ROW BEGIN
   UPDATE inventeries SET unit = unit + OLD.amount
   WHERE id = OLD.id_barang;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `bm`
--

CREATE TABLE `bm` (
  `id` int(11) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `catID` int(11) NOT NULL,
  `amount` varchar(222) NOT NULL,
  `userId` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bm`
--

INSERT INTO `bm` (`id`, `id_barang`, `catID`, `amount`, `userId`, `date`) VALUES
(4, 54, 31, '10', 1, '2022-01-11 12:13:45'),
(5, 55, 31, '5', 1, '2022-01-12 04:28:23'),
(6, 56, 31, '10', 1, '2022-01-12 04:28:33'),
(7, 57, 31, '15', 1, '2022-01-12 04:31:10'),
(8, 58, 31, '5', 1, '2022-01-12 04:32:20'),
(9, 59, 32, '10', 1, '2022-01-12 04:32:40'),
(10, 60, 32, '4', 1, '2022-01-12 04:33:35'),
(11, 60, 32, '1', 1, '2022-01-12 04:33:47'),
(12, 61, 32, '10', 1, '2022-01-12 04:34:02'),
(13, 62, 32, '20', 1, '2022-01-12 06:51:08'),
(14, 58, 31, '2', 1, '2022-01-12 06:52:04'),
(15, 60, 32, '2', 1, '2022-01-12 06:54:40'),
(16, 60, 32, '2', 1, '2022-01-12 06:54:51'),
(17, 60, 32, '1', 1, '2022-01-12 06:55:06'),
(18, 58, 31, '1', 1, '2022-01-12 06:55:26'),
(19, 58, 31, '1', 1, '2022-01-12 06:55:59'),
(20, 58, 31, '1', 1, '2022-01-12 06:58:04'),
(21, 55, 31, '1', 1, '2022-01-12 07:02:50'),
(22, 55, 31, '1', 1, '2022-01-12 07:03:17'),
(23, 55, 31, '1', 1, '2022-01-12 07:05:02'),
(24, 55, 31, '1', 1, '2022-01-12 07:06:12'),
(25, 55, 31, '1', 1, '2022-01-12 07:08:48'),
(26, 56, 31, '1', 1, '2022-01-12 07:11:17');

--
-- Triggers `bm`
--
DELIMITER $$
CREATE TRIGGER `barang_masuk` AFTER INSERT ON `bm` FOR EACH ROW BEGIN
   UPDATE inventeries SET unit = unit + NEW.amount
   WHERE id = NEW.id_barang;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `barang_masuk_delete` AFTER DELETE ON `bm` FOR EACH ROW BEGIN
   UPDATE inventeries SET unit = unit - OLD.amount
   WHERE id = OLD.id_barang;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(222) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `date`) VALUES
(31, 'Minyak Goreng', '2021-12-19 16:40:11'),
(32, 'Indomie', '2022-01-10 07:53:50'),
(33, 'Sarden', '2022-01-10 07:53:58'),
(34, 'Gula', '2022-01-10 07:54:03');

-- --------------------------------------------------------

--
-- Table structure for table `inventeries`
--

CREATE TABLE `inventeries` (
  `id` int(11) NOT NULL,
  `catId` int(11) NOT NULL,
  `name` text NOT NULL,
  `unit` text NOT NULL DEFAULT '0',
  `price` text NOT NULL DEFAULT '0',
  `userId` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inventeries`
--

INSERT INTO `inventeries` (`id`, `catId`, `name`, `unit`, `price`, `userId`, `date`) VALUES
(54, 31, 'Bimoli', '10', '10000', 1, '2022-01-10 08:36:13'),
(55, 31, 'Filma', '10', '15000', 1, '2022-01-12 04:26:58'),
(56, 31, 'Tropical', '11', '15000', 1, '2022-01-12 04:27:02'),
(57, 31, 'Sunco', '15', '0', 1, '2022-01-12 04:27:11'),
(58, 31, 'Sania', '10', '0', 1, '2022-01-12 04:27:14'),
(59, 32, 'Indomie Goreng 1Pcs', '10', '0', 1, '2022-01-12 04:27:33'),
(60, 32, 'Indomie Goreng 1 Dus', '10', '0', 1, '2022-01-12 04:27:43'),
(61, 32, 'Indomie Ayam Bawah 1Pcs', '10', '0', 1, '2022-01-12 04:27:56'),
(62, 32, 'Indomie Ayam Bawang 1 Dus', '20', '0', 1, '2022-01-12 04:28:09');

-- --------------------------------------------------------

--
-- Table structure for table `site`
--

CREATE TABLE `site` (
  `id` int(11) NOT NULL,
  `title` varchar(222) NOT NULL,
  `name` varchar(222) NOT NULL,
  `alamat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site`
--

INSERT INTO `site` (`id`, `title`, `name`, `alamat`) VALUES
(1, 'Tokoku Sejahtera', 'Tokoku Sejahtera', 'jalan tokoku');

-- --------------------------------------------------------

--
-- Table structure for table `struk`
--

CREATE TABLE `struk` (
  `id_strk` int(11) NOT NULL,
  `id_struk` text NOT NULL,
  `name` varchar(222) NOT NULL,
  `item` varchar(222) NOT NULL,
  `amount` varchar(222) NOT NULL,
  `userId` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `level` int(11) NOT NULL DEFAULT 2,
  `email` varchar(222) NOT NULL,
  `password` varchar(222) NOT NULL,
  `name` varchar(222) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `level`, `email`, `password`, `name`, `date`) VALUES
(1, 1, 'admin@admin.com', '21232f297a57a5a743894a0e4a801fc3', 'Admin', '2017-11-02 12:34:53'),
(9, 1, 'norman.kristiono@gmail.com', '9ca9a5bb22e0e6813091b0febf4f480c', 'Norman', '2021-12-18 13:43:44'),
(10, 2, 'user@user.com', 'ee11cbb19052e40b07aac0ca060c23ee', 'User', '2021-12-18 13:44:30');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bk`
--
ALTER TABLE `bk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bm`
--
ALTER TABLE `bm`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inventeries`
--
ALTER TABLE `inventeries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site`
--
ALTER TABLE `site`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `struk`
--
ALTER TABLE `struk`
  ADD PRIMARY KEY (`id_strk`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bk`
--
ALTER TABLE `bk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `bm`
--
ALTER TABLE `bm`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `inventeries`
--
ALTER TABLE `inventeries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `site`
--
ALTER TABLE `site`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `struk`
--
ALTER TABLE `struk`
  MODIFY `id_strk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
