<?php include 'include/head.php';?>
<?php include 'include/nav.php';?>
  <div class="content container">
  <a href="report_harian.php" class="btn btn-primary float-right"><i class="icon icon-back"></i> Kembali</a>
  <br/><br/>
  <div class="tableBox" >
    <table id="dataTable" class="table table-responsive table-hover">
      <thead>
        <th>#</th>
        <th>Date</th>
        <th>Jumlah Transaksi</th>
      </thead>
     <tbody>
      <?php $i=0;
          $array = $con->query("SELECT id, date, DAY(date) as tgl, MONTH(date) as bln, YEAR(date) as thn, COUNT(*) AS jumlah_trx
FROM bk
WHERE date=DATE(NOW())
GROUP BY date");
        while ($row = $array->fetch_assoc()) 
        { 
          $i=$i+1;
          $id = $row['id'];
        ?>
          <tr>
            <td><?php echo $i; ?></td>
            <td><?php echo $row['date']; ?></td>
            <td><a href="report_harian_ini_detail.php?year=<?php echo $row['thn']; ?>&month=<?php echo $row['bln']; ?>&day=<?php echo $row['tgl']; ?>"><?php echo $row['jumlah_trx']; ?></a></td>
          </tr>
      <?php
        }
       ?>
     </tbody>
    </table>

  </div>                      

  </div>  
    <?php include 'include/footer.php';?>