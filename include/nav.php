<header class="navbar shadow">
   
<!--  <section class="navbar-section">
    <a href="cat.php" class="btn btn-link">Youtube</a>
  </section> -->
   
  <section class="navbar-center">
    <!-- centered logo or brand -->

	<div class="dropdown">
  <a href="#" class="btn btn-primary dropdown-toggle" tabindex="0">
 Menu
  </a>
  <!-- menu component -->
 <ul class="menu nav">
  <li class="nav-item">
    <a href="index.php" class="text-primary"><i class="icon icon-apps"></i> Beranda</a>
  </li>
  <li class="nav-item">
    <a href="manageCat.php" class="text-primary"><i class="icon icon-bookmark"></i> Kategori</a>
  </li>
  <li class="nav-item">
    <a href="products.php" class="text-primary"><i class="icon icon-link"></i> Produk</a>
  </li>
    <li class="nav-item">
    <a href="pos.php" class="text-primary"><i class="icon icon-photo"></i> POS</a>
  </li>
  <?php if($_SESSION['level'] == 1){ ?>
    <li class="nav-item">
    <a href="reports.php" class="text-primary"><i class="icon icon-time"></i> Laporan</a>
  </li>
  <li class="nav-item">
    <a href="setting.php" class="text-primary"><i class="icon icon-more-horiz"></i> Konfigurasi</a>
  </li>
  <?php }else{}?>
  <li class="nav-item">
    <a href="logout.php" class="text-primary"><i class="icon icon-shutdown"></i> Keluar</a>
  </li>
</ul>
</div>
	<!-- end -->
  </section>
 <!--  <section class="navbar-section">
    <a href="#" class="btn btn-link">GitHub</a>
  </section> -->
</header>
