<?php
// Set header type konten.
header("Content-Type: application/json; charset=UTF-8");

// Deklarasi variable untuk koneksi ke database.
$host     = "localhost"; // Server database
$username = "root"; // Username database
$password = "root12345"; // Password database
$database = "axcoraspectre"; // Nama database

// Koneksi ke database.
$conn = new mysqli($host, $username, $password, $database);

// Deklarasi variable keyword nmbrg.
$nmbrg = $_GET["query"];

// Query ke database.
$query  = $conn->query("SELECT * FROM inventeries WHERE name LIKE '%$nmbrg%' ORDER BY nmbrg DESC");
$result = $query->fetch_all(MYSQLI_ASSOC);

// Format bentuk data untuk autocomplete.
foreach($result as $data) {
    $output['suggestions'][] = [
        //'value' => $data['nmbrg'],
        'nmbrg'  => $data['name']
    ];
}

if (! empty($output)) {
    // Encode ke format JSON.
    echo json_encode($output);
}