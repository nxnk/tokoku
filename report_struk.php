<?php include 'include/head.php';?>
<?php include 'include/nav.php';?>
  <div class="content container">
  <a href="reports.php" class="btn btn-primary float-right"><i class="icon icon-back"></i> Kembali</a>
  <br/><br/>
  <div class="tableBox" >
    <table id="dataTable" class="table table-responsive table-hover">
      <thead>
        <th>#</th>
        <th>Id Struk</th>
        <th>Nama Pembeli</th>
        <th>Jumlah Item</th>
        <th>Total harga</th>
        <th>Staff</th>
        <th>Date</th>
        <!--<th></th>-->
        
      </thead>
     <tbody>
      <?php $i=0;
          $array = $con->query("
		  SELECT 
id_strk,
id_struk,
name as nama_pembeli,
item as jml_item,
amount as ttl_harga,
userId,
date
FROM struk
ORDER BY date DESC");
        while ($row = $array->fetch_assoc()) 
        { 
          $i=$i+1;
          $id = $row['id_strk'];
        ?>
          <tr>
            <td><?php echo $i; ?></td>
            <td><a href="report_struk_detail.php?id_struk=<?php echo $row['id_struk'] ?>"><?php echo $row['id_struk']; ?></a></td>
            <td><?php echo $row['nama_pembeli']; ?></td>
            <td><?php echo $row['jml_item']; ?></td>
            <td><?php echo rupiah($row['ttl_harga']); ?></td>
            <td><?php echo getAdminName($row['userId']); ?></td>
            <td><?php echo $row['date']; ?></td>
			<!--<td>
				<a href="delete.php?item_bk=<?php echo $row['id'] ?>"><button class='btn btn-danger'>Delete Item</button></a>
			</td>-->
          </tr>
      <?php
        }
       ?>
     </tbody>
    </table>

  </div>                      

  </div>  
    <?php include 'include/footer.php';?>