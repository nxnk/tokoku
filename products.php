<?php include 'include/head.php';?>
<?php include 'include/nav.php';?>
<?php 
if (isset($_GET['catId']))
{
  $catId = $_GET['catId'];
  $array = $con->query("select * from categories where id='$catId'");
 
}
else
{
  $catName = "All Inventeries";
  $stockArray = $con->query("select * from inventeries");
}
  //include 'assets/bill.php';
 ?>
  <div class="content container">
  <?php echo $notice ?>
  <a href="addnew.php" class="btn btn-primary float-right"><i class="icon icon-plus"></i> New Item</a>
  <!--<button class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#addProd"><i class="icon icon-plus"></i> New Item</button>-->
  <br/><br/>
  <div class="tableBox" >
    <table id="dataTable" class="table table-striped table-hover">
      <thead>
        <th>#</th>
        <th>Id Produk</th>
        <th>Kategori Id</th>
        <th>Nama Produk</th>
		<th>Deskripsi</th>
        <th>Unit</th>
        <th>Harga</th>
        <th></th>
      </thead>
     <tbody>
      <?php $i=0;
        while ($row = $stockArray->fetch_assoc()) 
        { 
          $i=$i+1;
          $id = $row['id'];
        ?>
          <tr>
			<td id="kesini-<?php echo $row['id']; ?>"><?php echo $i; ?></td>
			<td><?php echo $row['id']; ?></td>
			<td><?php echo $row['catId']; ?></td>
			<td><?php echo $row['name']; ?></td>
			<td><?php echo $row['description']; ?></td>
			<td><?php echo $row['unit']; ?></td>
			<td> <?php echo rupiah($row['price']);?></td>
			<td colspan="center">
				<a href="products_Ubah.php?id=<?php echo $row['id'] ?>"><button class='btn btn-primary'>Ubah Barang</button></a>
				<a href="addStok.php?id=<?php echo $row['id'] ?>"><button class='btn btn-primary'>Tambah Stok</button></a>
				<a href="addHarga.php?id=<?php echo $row['id'] ?>"><button class='btn btn-primary'>Ubah Harga</button></a>
				<a onclick="return konfirmasi()" href="delete.php?item=<?php echo $row['id'] ?>"><button class='btn btn-danger'>Hapus</button></a>
			</td>
          </tr>
      <?php
        }
       ?>
     </tbody>
    </table>
  </div>                      

  </div>
  
  <div id="addProd" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Produk Baru</h4>
      </div>
      <div class="modal-body"> 
	  <form method="POST" enctype="multipart/form-data">
        
		    <div class="form-group">
				<label for="some" class="col-form-label">Select Category</label>
				<select class="form-control" required name="catId">
				  <option selected disabled value="">Please Select Category</option>
				<?php getAllCat(); ?>
				  
				</select>
			</div>
			<div class="form-group">
				<label for="some" class="col-form-label">Nama Barang</label>
				<input type="text" name="name" class="form-control" id="nameBrg" required>
			</div>
			<div class="form-group">
				<label for="some" class="col-form-label">Deskripsi Barang</label>
				<input type="text" name="desc" class="form-control" id="desc" required>
			</div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary" name="saveProduct">Simpan</button>
      </div>
    </form>
    </div>

  </div>
</div>

  <?php include 'include/footer.php';?>