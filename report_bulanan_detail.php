<?php include 'include/head.php';?>
<?php include 'include/nav.php';?>
  <div class="content container">
  <a href="report_bulanan.php" class="btn btn-primary float-right"><i class="icon icon-back"></i> Kembali</a>
  <br/><br/>
  <div class="tableBox" >
    <table id="dataTable" class="table table-responsive table-hover">
      <thead>
        <th>#</th>
        <th>Id Barang Keluar</th>
        <th>Id Barang</th>
        <th>Kategori ID</th>
        <th>Nama Barang</th>
        <th>Jumlah</th>
        <th>Id Struk</th>
      </thead>
     <tbody>
      <?php 
		$i=0;
		$month = $_GET['month'];
		$year = $_GET['year'];
          $array = $con->query("SELECT * FROM bk WHERE YEAR(date) = '$year' AND MONTH(date) = '$month'");
          while ($row = $array->fetch_assoc()) 
        { 
          $i=$i+1;
          $id = $row['id'];
        ?>
          <tr>
            <td><?php echo $i; ?></td>
            <td><?php echo $row['id']; ?></td>
            <td><?php echo $row['id_barang']; ?></td>
            <td><?php echo $row['catID']; ?></td>
            <td><?php echo $row['name']; ?></td>
            <td><?php echo $row['amount']; ?></td>
            <td><?php echo $row['id_struk']; ?></td>
          </tr>
      <?php
        }
       ?>
     </tbody>
    </table>

  </div>                      

  </div>  
    <?php include 'include/footer.php';?>