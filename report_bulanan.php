<?php include 'include/head.php';?>
<?php include 'include/nav.php';?>
  <div class="content container">
  <a href="report_bulanan_ini.php" class="btn btn-primary float-left">Report Bulan ini</a> 
  <a href="reports.php" class="btn btn-primary float-right"><i class="icon icon-back"></i> Kembali</a>
  <br/><br/>
  <div class="tableBox" >
    <table id="dataTable" class="table table-responsive table-hover">
      <thead>
        <th>#</th>
        <th>Tahun/Bulan</th>
        <th>Jumlah Transaksi</th>
      </thead>
     <tbody>
      <?php $i=0;
			$array = $con->query("
				SELECT id, CONCAT(YEAR(date),'/',MONTH(date)) AS tahun_bulan, CONCAT(YEAR(date)) as tahun, CONCAT(MONTH(date)) as bulan , COUNT(*) AS jumlah_trx FROM bk GROUP BY YEAR(date),MONTH(date)
			");
        while ($row = $array->fetch_assoc()) 
        { 
          $i=$i+1;
          $id = $row['id'];
        ?>
          <tr>
            <td><?php echo $i; ?></td>
            <td><?php echo $row['tahun_bulan']; ?></td>
            <td><a href="report_bulanan_detail.php?month=<?php echo $row['bulan']; ?>&year=<?php echo $row['tahun']; ?>"><?php echo $row['jumlah_trx']; ?></a></td>
          </tr>
      <?php
        }
       ?>
     </tbody>
    </table>

  </div>                      

  </div>  
    <?php include 'include/footer.php';?>