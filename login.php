<?php require "assets/function.php" ?>
<?php require 'assets/db.php';?>
<!DOCTYPE html>
<html>
<head>
	<title>Login</title>
	<?php require "assets/autoloader.php" ?>
	<style type="text/css">
	<?php include 'css/customStyle.css'; ?>
	</style>
	<!-- CSS only -->
<link href="css/bootstrap.css" rel="stylesheet" integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0" crossorigin="anonymous">
 <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<div class="container">
<div class="row">
<div class="col-12  col-md-3"></div>
<div class="col-12 col-md-6 p-3 p-md-5" style="margin: 20% 0px;">
	<div class="login-box text-center p-3 p-md-5">
  	<h3 class="text-center">Login</h3>

  <!-- /.login-logo -->
 
    <p class="login-box-msg"></p>
    <form action="" method="post">
      <div class="form-group has-feedback">
        <input type="email" name="email" class="form-control" placeholder="Email" required>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" name="password" class="form-control" placeholder="Password" required>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
          <button type="submit" name="login" class="btn btn-primary btn-block btn-lg">Log In</button>
    </form>
  </div>
  <br>
  <div class="alert alert-danger" id="error"  style="width: 25%;margin: auto;display: none;"></div>
<?php 

if (isset($_POST['login'])) 
{
	$user = $_POST['email'];
    $pass = md5($_POST['password']);

    $result = $con->query("select * from users where email='$user' AND password='$pass'");
    if($result->num_rows>0)
    {	
    	session_start();
    	$data = $result->fetch_assoc();
    	$_SESSION['userId'] = $data['id'];
		$_SESSION['level'] = $data['level'];
		$_SESSION['bill'] = array();
    	header('location:index.php');
      }
    else
    {
     	echo 
     	"<script>
     		\$(document).ready(function(){\$('#error').slideDown().html('Login Error! Try again.').delay(3000).fadeOut();});
     	</script>
     	";
    }
}

 ?>
</div>
</div>
</body>
</html>
