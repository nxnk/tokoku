<?php include 'include/head.php';?>
<?php include 'include/nav.php';?>
  <div class="content container">
  <a href="report_harian_ini.php" class="btn btn-primary float-left">Report Hari ini</a> 
  <a href="reports.php" class="btn btn-primary float-right"><i class="icon icon-back"></i> Kembali</a>
  <br/><br/>
  <div class="tableBox" >
	<table id="dataTable" class="table table-responsive table-hover">
      <thead>
        <th>#</th>
        <th>Date</th>
        <th>Jumlah Transaksi</th>
      </thead>
     <tbody>
      <?php $i=0;
          $array = $con->query("SELECT id,date,COUNT(*) AS jumlah_trx FROM bk GROUP BY date ORDER BY date DESC");
        while ($row = $array->fetch_assoc()) 
        { 
          $i=$i+1;
          $id = $row['id'];
        ?>
          <tr>
            <td><?php echo $i; ?></td>
            <td><?php echo $row['date']; ?></td>
            <td><a href="report_harian_detail.php?date=<?php echo $row['date']; ?>"><?php echo $row['jumlah_trx']; ?></a></td>
          </tr>
      <?php
        }
       ?>
     </tbody>
    </table>
  </div>
  </div>  
    <?php include 'include/footer.php';?>